export default function () {
  const invitation = {
    // data
    couple: {
      groom: {
        fullName: "Eko Ardyanto, S.Kom.",
        nickName: "Eko",
        father: "Sudar",
        mother: "Tasiyem",
        // img: "couple/couple8.jpg",
        img: "https://source.unsplash.com/random/?city,night",
      },
      bride: {
        fullName: "Ghassani Razan Gafnie, S.Pi.",
        nickName: "Rosa",
        father: "Abdul Gafar Basnie (Alm)",
        mother: "Nur Aini",
        // img: "couple/couple7.jpg",
        img: "https://source.unsplash.com/random/?city,night",
      },
    },
    verse: {
      verse:
        "Dan di antara tanda-tanda (kebesaran)-Nya ialah Dia menciptakan pasangan-pasangan untukmu dari jenismu sendiri, agar kamu cenderung dan merasa tenteram kepadanya, dan Dia menjadikan di antaramu rasa kasih dan sayang.",
      chapter: "Ar-Rum: 21",
    },
    events: [
      {
        name: "Akad",
        venue: "Masjid Al-Amin",
        location: "Tembesi, Kec. Batu Aji, Kota Batam",
        mapsURL: "https://goo.gl/maps/iKCEgFNLKmtFufEU8",
        iframeURL:
          "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3989.162984382721!2d103.9926260749652!3d1.0384820989515278!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31d98def7f6fecdf%3A0xca8091f2c09bb05b!2sMasjid%20Al-Amin%20Tembesi!5e0!3m2!1sen!2sid!4v1690944060168!5m2!1sen!2sid",
        img: "",
        date: {
          timestamp: "",
          raw: "Minggu, 20 Agustus 2023",
          day: "Sun",
          date: "20",
          month: "Aug",
          year: "2023",
          start: "10:00",
          end: "11:00",
          tz: "WIB",
        },
      },
      {
        name: "Resepsi",
        venue: "Aula Masjid Al-Amin",
        location: "Tembesi, Kec. Batu Aji, Kota Batam",
        mapsURL: "https://goo.gl/maps/iKCEgFNLKmtFufEU8",
        iframeURL:
          "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3989.162984382721!2d103.9926260749652!3d1.0384820989515278!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31d98def7f6fecdf%3A0xca8091f2c09bb05b!2sMasjid%20Al-Amin%20Tembesi!5e0!3m2!1sen!2sid!4v1690944060168!5m2!1sen!2sid",
        img: "",
        date: {
          timestamp: "",
          raw: "Minggu, 20 Agustus 2023",
          day: "Sun",
          date: "20",
          month: "Aug",
          year: "2023",
          start: "13:00",
          end: "17:00",
          tz: "WIB",
        },
      },
    ],
    saveTheDate: {
      countdown: "2023-08-20 10:00:00",
      link: "https://calendar.google.com/calendar/event?action=TEMPLATE&tmeid=NWhsMmlnanZodWlvOWkzN3M2ZWQwYWZpMmkgdGVhbXNvZXJwbHVzQG0&tmsrc=teamsoerplus%40gmail.com",
    },
    stories: [
      {
        name: "First Meet",
        date: "1st January 2022",
        img: "https://source.unsplash.com/random/?city,night",
        story:
          "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum, iure consectetur blanditiis exercitationem, corrupti rerum corporis provident doloribus sit doloremque cum asperiores.",
      },
      {
        name: "First Date",
        date: "1st March 2022",
        img: "https://source.unsplash.com/random/?city,night",
        story:
          "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum, iure consectetur blanditiis exercitationem, corrupti rerum corporis provident doloribus sit doloremque cum asperiores.",
      },
      {
        name: "Proposal",
        date: "1st October 2022",
        img: "https://source.unsplash.com/random/?city,night",
        story:
          "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum, iure consectetur blanditiis exercitationem, corrupti rerum corporis provident doloribus sit doloremque cum asperiores.",
      },
    ],
    gallery: {
      baseUrl: "https://eko-rosa.mengundang-nikah.id/",
      img: [
        "couple/rosa1.jpeg",
        "couple/rosa2.jpeg",
        "couple/rosa3.jpeg",
        "couple/rosa4.jpeg",
        "couple/rosa5.jpeg",
        "couple/rosa6.jpeg",
        "couple/rosa7.jpeg",
        "couple/rosa8.jpeg",
        "couple/rosa9.jpeg",
        "couple/rosa10.jpeg"
      ],
      thumbnail: [
        "couple/rosa1.jpeg",
        "couple/rosa2.jpeg",
        "couple/rosa3.jpeg",
        "couple/rosa4.jpeg",
        "couple/rosa5.jpeg",
        "couple/rosa6.jpeg",
        "couple/rosa7.jpeg",
        "couple/rosa8.jpeg",
        "couple/rosa9.jpeg",
        "couple/rosa10.jpeg"
      ],
    },
    gift: {
      transfers: [
        {
          issuer: "Mandiri",
          recipient: "Eko Ardyanto",
          referenceNumber: "10 900 2062 6818",
          img: "/misc/mandiri-logo.png",
        },
      ],
      hampers: [
        {
          name: "Kediaman Mempelai Wanita",
          phoneNo: "085668813126",
          location: " Perum. Muka Kuning Indah 1 blok AL nomor 5, Kel. Buliang, Kec. Batu Aji, Kota Batam. 29424.",
        },
      ],
    },
    rsvp: {
      restricted: false,
      id: "",
      name: "",
      attending: "",
      numberOfPerson: "",
    },
    copyright: {
      name: "Mengundang Nikah",
      url: "https://www.instagram.com/mengundangnikah.id/",
    },

    // api call config
    api: {
      baseUrl: "https://api.mengundang-nikah.id",
      invitationID: "8",
      path: {
        csrf: "/sanctum/csrf-cookie",
        rsvp: "/api/undangan/rsvp",
        wishes: "/api/addComment",
        getWishes: "/api/getComment",
      },
    },
  };
  return useState("invitation", () => invitation);
}
