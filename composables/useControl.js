export default function () {
  const control = {
    nav: {
      show: false,
    },
    body: {
      open: false,
    }
  };
  return useState("control", () => control);
}
