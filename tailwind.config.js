/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./components/**/*.{js,vue,ts}",
    "./layouts/**/*.vue",
    "./pages/**/*.vue",
    "./plugins/**/*.{js,ts}",
    "./nuxt.config.{js,ts}",
  ],
  theme: {
    extend: {
      fontFamily: {
        primary: "Astheresia, sans-serif",
        secondary: "Avida La Bila, sans-serif",
        body: "Ignacio Maverick, serif",
      },
      colors: {
        primary: "#034488",
        secondary: "#178fd6",
        accent: "#edece8",
        light: "#fffcf6",
      },
    },
  },
  plugins: [],
};
