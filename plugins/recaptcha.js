import { VueReCaptcha } from "vue-recaptcha-v3";

export default defineNuxtPlugin((nuxtApp) => {
  nuxtApp.vueApp.use(VueReCaptcha, {
    siteKey: "6Ld4Y0UgAAAAAPJK5otVSN6DDV-auzSoalH9AQRs",
  });
});
