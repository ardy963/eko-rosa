// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  modules: ["@nuxtjs/tailwindcss"],
  plugins: [
    { src: "@/plugins/aos", ssr: false, mode: "client" },
    { src: "@/plugins/recaptcha", ssr: false, mode: "client" },
  ],
  ssr: false,
  app: {
    head: {
      title: "Eko and Rosa Wedding Invitation",
      link: [{ rel: "icon", href: "/favicon/favicon.ico" }],
      meta: [
        {
          property: "og:site_name",
          content: "Eko and Rosa Wedding Invitation",
        },
        {
          property: "og:title",
          content: "Eko and Rosa Wedding Invitation",
        },
        { property: "og:description", content: "You are invited to our wedding!" },
        {
          property: "og:image",
          content: "https://eko-rosa.mengundang-nikah.id/couple/meta-image.jpg",
        },
        { property: "og:type", content: "website" },
      ],
    },
  },
});
